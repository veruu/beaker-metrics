"""Convert influx line protocol datapoints into SQL INSERT queries."""
from datetime import datetime
from datetime import timedelta


def _get_valuekey_str(result_str, sample_view):
    try:
        if result_str:
            result_str += ', '
        result_str += ', '.join(sample_view)
    except KeyError:
        pass

    return result_str


def guess_delim(item):
    """Quote string using appropriate delimiter."""
    res = str(item)
    if "'" in res:
        res = res.replace("'", "''")

    return f"'{res}'"


def guess_convert(sample_view):
    """Convert datatypes from influx line protocol to SQL INSERT."""
    result = []
    for item in sample_view:
        if isinstance(item, str):
            if not item.strip():
                result.append('NULL')
            else:
                result.append(guess_delim(item))
        elif isinstance(item, datetime):
            result.append(guess_delim(item))
        elif isinstance(item, int):
            result.append(str(item))
        elif isinstance(item, timedelta):
            result.append(str(item.total_seconds()))
        elif isinstance(item, float):
            result.append(str(item))
        else:
            print(type(item))
            raise RuntimeError('unhandled case')

    return result


def insert2pgsql(samples):
    """Create INSERT queris from datapoints."""
    # use 1 sample to get schema name
    schema_name = samples[0]['measurement']

    keys_of_tags = [str(x) for x in samples[0]['tags'].keys()]
    keys_of_fields = [str(x) for x in samples[0]['fields'].keys()]
    keys_str = _get_valuekey_str('', keys_of_tags)
    keys_str = _get_valuekey_str(keys_str, keys_of_fields)
    if not keys_str:
        raise RuntimeError('no tags/fields found!')

    queries = []
    for sample in samples:
        values_arr = []
        values_tags = guess_convert(sample['tags'].values())
        values_of_fields = guess_convert(sample['fields'].values())
        values_str = _get_valuekey_str('', values_tags)
        values_str = _get_valuekey_str(values_str, values_of_fields)
        timestamp = sample['time']

        values_arr.append(f"('{timestamp}', {values_str})")

        query = f'''INSERT INTO {schema_name}(time, {keys_str})
         VALUES {', '.join(values_arr)};'''
        queries.append(query)

    return queries
