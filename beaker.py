"""Pull data from Beaker teiid."""
import itertools
import math
import sys
from datetime import datetime
from datetime import timedelta

import tqdm
from dateutil.parser import parse as date_parse

from cki_lib.logger import file_logger

LOGGER = file_logger(__name__)


def sanitize_date(datestr):
    """Convert datestring to datetime, interpret NULLs as epoch_date."""
    if isinstance(datestr, type(None)):
        return get_epoch_date()
    if isinstance(datestr, datetime):
        return datestr
    try:
        return date_parse(datestr)
    except ValueError:
        if 'Strinf does not contain a data' in str(sys.exc_info()[1]):
            raise
        return get_epoch_date()


def get_epoch_date():
    """Return epoch date in datetime datatype."""
    return date_parse('January 1, 1970, 00:00:00')


class BkrBase:
    """A base for other Beaker objects, simplifies coding."""

    def __init__(self, **kwargs):
        """Create the object."""
        self.kwargs = kwargs
        for key, val in self.kwargs.items():
            setattr(self, key, val)

    @classmethod
    def from_rows_cols(cls, rows, cols):
        """Create a list of instances with kwargs/dict filled by rows:cols."""
        return [cls(**dict(zip(cols, row))) for row in rows]

    def __getitem__(self, key):
        """Retrieve item using a key."""
        return self.__dict__[key]


class BkrParams(BkrBase):
    """Wrapper to get Beaker parameters of a task."""

    @classmethod
    def from_query(cls, connector, task_id_list):
        """Query Beaker, input a list of taskids, get a list of params."""
        query = f"""SELECT "name", recipe_task_id, "value" FROM
        recipe_task_param WHERE recipe_task_id
        IN ({','.join(task_id_list)});"""

        return cls.from_rows_cols(*connector.query(query))


class BkrInstallations(BkrBase):
    """Wrapper for getting data about Beaker systems installations."""

    def __init__(self, connector, **kwargs):
        """Create the object."""
        super(BkrInstallations, self).__init__(**kwargs)
        self.connector = connector

    def from_job(self, job_kwargs):
        # pylint: disable=R0914
        """Query Beaker, get datapoints of installations and commands."""
        install_cmds_results = []
        installation_results = []
        kwargs = job_kwargs

        query = f'''SELECT cmd.id, cmd.status, cmd.action,
        cmd.queue_time, cmd.start_time, cmd.finish_time,
        inst.recipe_id, inst.id,
        inst.created, inst.postinstall_finished,
        distro.name, arch.arch
        FROM command_queue AS cmd
        LEFT JOIN installation AS inst ON cmd.installation_id = inst.id
        LEFT JOIN arch ON arch.id = inst.arch_id
        LEFT JOIN distro_tree ON inst.distro_tree_id = distro_tree.id
        LEFT JOIN distro ON distro.id = distro_tree.distro_id
        WHERE inst.recipe_id IN ({','.join(kwargs['lst_recipe_ids'])})'''

        rows, _ = self.connector.query(query)

        # there are multiple cmd_action's that are done per 1 installation,
        # skip over already processed recipeids for installations, but not for
        # installation commands
        processed_recipe_ids = set()
        for cmd_id, cmd_status, cmd_action, cmd_queue_time, cmd_start_time, \
            cmd_finish_time, recipe_id, installation_id, created, \
                postinstall_finished, distro_name, arch in rows:

            cmd_start_time = sanitize_date(cmd_start_time)
            cmd_finish_time = sanitize_date(cmd_finish_time)
            cmd_duration = (cmd_finish_time - cmd_start_time).total_seconds()

            inst_start_tm = sanitize_date(created)
            inst_finish_tm = sanitize_date(postinstall_finished)
            inst_duration = (inst_finish_tm - inst_start_tm).total_seconds()
            if recipe_id not in processed_recipe_ids:
                installation_results.append({
                    'measurement': 'installations',
                    'tags': {
                        'arch': arch,
                        'distro_name': distro_name,
                        'group_name': kwargs['group_name'],
                        'owner_name': kwargs['owner_name'],
                    },
                    'fields': {
                        'id': installation_id,
                        'job_id': kwargs['job_id'],
                        'recipe_id': recipe_id,
                        'duration': inst_duration,
                    },
                    'time': cmd_queue_time or kwargs['queue_time'],
                })
                processed_recipe_ids.add(recipe_id)

            install_cmds_results.append({
                'measurement': 'installation_commands',
                'tags': {
                    'arch': arch,
                    'distro_name': distro_name,
                    'status': cmd_status,
                    'action': cmd_action,
                    'group_name': kwargs['group_name'],
                    'owner_name': kwargs['owner_name'],
                },
                'fields': {
                    'id': cmd_id,
                    'job_id': kwargs['job_id'],
                    'recipe_id': recipe_id,
                    'duration': cmd_duration,
                },
                'time': cmd_start_time or cmd_queue_time,
            })

        return [install_cmds_results, installation_results]


class BkrTask(BkrBase):
    """Wrapper for getting Beaker task."""

    def __init__(self, **kwargs):
        """Create the object."""
        super(BkrTask, self).__init__(**kwargs)

        self.params = []

        self.waived = False
        self.maintainer_list = []

    @classmethod
    def from_query_filtered(cls, connector, recipe_id):
        """Get tasks from Beaker recipe data."""
        # Some tasks we don't really care about.
        excluded_tasks = [
            '/distribution/check-install',
            '/distribution/install',
            '/distribution/command',
            '/test/misc/machineinfo'
        ]
        bkr_tasks = cls.from_query(connector, recipe_id)

        return [task for task in bkr_tasks if task.name not in excluded_tasks]

    @classmethod
    def from_query(cls, connector, recipe_id):
        """Query Beaker, input recipe_id, get a list of Beaker tasks."""
        query = f'''SELECT recipe_task.name, result, status,
                recipe_task.id AS task_id, fqdn, fetch_url, start_time,
                finish_time
                FROM recipe_task
                LEFT JOIN recipe_resource ON
                recipe_resource.recipe_id = recipe_task.recipe_id
                WHERE recipe_task.recipe_id = {recipe_id};'''
        rows, cols = connector.query(query)
        bkr_tasks = cls.from_rows_cols(rows, cols)
        bkr_all_params = BkrParams.from_query(connector,
                                              [str(task['task_id']) for task
                                               in bkr_tasks])
        for i, _ in enumerate(bkr_tasks):
            task = bkr_tasks[i]
            task.params = [param for param in bkr_all_params if
                           param['recipe_task_id'] == task['task_id']]

            for par in task.params:
                if par.name == 'CKI_WAIVED' and par.value.lower() == 'true':
                    task.waived = True
                elif par.name == 'CKI_MAINTAINERS':
                    #  Get the test maintainers for this task.
                    task.maintainer_list = par.value.split(', ')

        return bkr_tasks


class BkrMetricsJob(BkrBase):
    """Wrapper for processing Beaker data into beaker-metrics tables."""

    def __init__(self, connector, **kwargs):
        """Create the object."""
        super(BkrMetricsJob, self).__init__(**kwargs)
        self.connector = connector

        self.max_id = 0
        self.stop_at = 0
        self.batchsize = 200
        self.i = 0
        self.pbar = None
        self.max_batch_index = 0

    def set_batch2process(self, i, stop_at):
        """Tell the object where to continue processing."""
        self.i = i
        self.stop_at = stop_at
        if not self.max_id:
            self.compute_batches()
        self.pbar.update(i)

    def compute_batches(self):
        """Determine in how big of a batch will we process the data."""
        rows, _ = self.connector.query('SELECT COUNT(*) FROM job;')
        self.max_id = [int(x) for x in itertools.chain(*rows)][0]
        self.max_batch_index = math.ceil(self.max_id / self.batchsize)
        self.pbar = tqdm.tqdm(total=self.max_batch_index)

    def get_batch_of_ids(self):
        """Wrap processing; keep calling this until is_done is True."""
        is_done = False
        if not self.max_id:
            self.compute_batches()

        data = self.get_next_batch()

        LOGGER.info(f'processing batch {self.i}/{self.max_batch_index}...')

        self.i += 1
        self.pbar.update(1)
        if self.i == self.max_batch_index:
            is_done = True
            self.pbar.close()

        return data, is_done

    def get_next_batch(self):
        """Get next batch of ids (raw data) to process from db."""
        rows, _ = self.connector.query(f'SELECT id FROM job WHERE id '
                                       f'> {self.stop_at} ORDER BY id'
                                       f' ASC LIMIT {self.batchsize};')

        data = [int(x) for x in itertools.chain(*rows)]
        self.stop_at = data[-1]

        return data

    def get_job_ids(self):
        """Get jobids from our target database (pgsql)."""
        rows, _ = self.connector.query('SELECT id FROM job ORDER BY id ASC ;')

        return [int(x) for x in itertools.chain(*rows)]

    def get_resources(self):
        # pylint: disable=R0914
        """Query Beaker, input list of recipeids, get resources datapoints."""
        results = []
        kwargs = self.kwargs

        query = f'''SELECT DISTINCT recipe.id, recipe.status, recipe.result,
        recipe_resource.fqdn, Beaker.system.vendor, Beaker.system.memory,
        cpu.vendor, (SELECT COUNT(d.id) FROM disk AS d WHERE disk.id = d.id),
        system.date_added,
        (SELECT MAX(disk.size) FROM disk WHERE
        Beaker.system.id = disk.system_id),
        (SELECT MAX(cpu.processors) FROM cpu WHERE
        Beaker.system.id = cpu.system_id)
        FROM recipe LEFT JOIN recipe_resource
            ON recipe.id = recipe_resource.recipe_id
        LEFT JOIN Beaker.system
            ON Beaker.system.fqdn = recipe_resource.fqdn
        LEFT JOIN cpu
            ON Beaker.system.id = cpu.system_id
        LEFT JOIN disk
            ON Beaker.system.id = disk.system_id
        WHERE recipe.id IN ({','.join(kwargs['lst_recipe_ids'])})
        '''

        rows, _ = self.connector.query(query)
        for recipe_id, status, result, fqdn, system_vendor, system_memory, \
            cpu_vendor, system_disk_count, system_date_added, \
                system_disk_space, cpu_processors in rows:

            results.append({
                'measurement': 'resources',
                'tags': {
                    'fqdn': fqdn,
                    'group_name': kwargs['group_name'],
                    'owner_name': kwargs['owner_name'],
                    'status': status,
                    'result': result,
                },
                'fields': {
                    'vendor': system_vendor,
                    'cpu_vendor': cpu_vendor,
                    'memory': system_memory,
                    'disk_count': system_disk_count,
                    'disk_space': system_disk_space,
                    'cpu_processors': cpu_processors,
                    'created_date': sanitize_date(system_date_added),
                    'job_id': kwargs['job_id'],
                    'recipe_id': recipe_id,
                },
                'time': kwargs['queue_time'],
            })
        return results

    def get_metrics_job_sample(self):
        """Query Beaker, input list of recipeids, get jobs datapoints."""
        kwargs = self.kwargs
        try:
            max_duration = max(kwargs['rec_durations'])
        except ValueError:
            max_duration = timedelta(0).total_seconds()

        # get queue times of recipeids
        query = f'''SELECT max(command_queue.queue_time) FROM command_queue
         LEFT JOIN installation ON installation.id =
         command_queue.installation_id WHERE installation.recipe_id IN
         ({', '.join(kwargs['lst_recipe_ids'])});'''
        rows, _ = self.connector.query(query)
        max_queue_time = abs(kwargs['queue_time'] - sanitize_date(rows[0][0]))

        return {
            'measurement': 'jobs',
            'tags': {
                'group_name': kwargs['group_name'],
                'owner_name': kwargs['owner_name'],
                'status': kwargs['status'],
            },
            'fields': {
                'id': kwargs['job_id'],
                'whiteboard': kwargs['whiteboard'],
                'queue_time': max_queue_time,
                'duration': max_duration,
            },
            'time': kwargs['queue_time']
        }

    def get_recipeids_status_distro_arch(self, lst_recipe_ids):
        """Query Beaker, input list of recipeids, get status, distro, arch."""
        query = f'''SELECT status, distro.name, arch.arch
         FROM recipe LEFT JOIN distro_tree ON
         recipe.distro_tree_id = distro_tree.id
         LEFT JOIN distro ON distro.id = distro_tree.distro_id
         LEFT JOIN arch ON arch.id = distro_tree.arch_id
         WHERE recipe.id IN ({','.join(lst_recipe_ids)})'''

        statuses, distro_names, arches = [], [], []
        rows, _ = self.connector.query(query)
        for status, distro_name, arch in rows:
            statuses.append(status)
            distro_names.append(distro_name)
            arches.append(arch)

        return statuses, distro_names, arches

    def get_machine_recipes(self):
        """Convert internaldata for recipeids to machine_recipes datapoints."""
        result = []
        kwargs = self.kwargs

        src = zip(kwargs['lst_recipe_ids'], kwargs['rec_durations'],
                  kwargs['rec_start_times'], kwargs['statuses'],
                  kwargs['distro_names'], kwargs['arches'])
        for recipe_id, duration, start_time, status, distro_name, arch in src:
            result.append({
                'measurement': 'machine_recipes',
                'tags': {
                    'status': status,
                    'arch': arch,
                    'distro_name': distro_name,
                    'group_name': kwargs['group_name'],
                    'owner_name': kwargs['owner_name'],
                },
                'fields': {
                    'job_id': kwargs['job_id'],
                    'recipe_id': recipe_id,
                    # 'position_in_job': position_in_job,
                    'duration': duration,
                },
                'time': start_time or kwargs['queue_time'],
            })
        return result

    def get_metrics_part(self, job_id):
        """Query Beaker, get group, owner, status, whiteboard, time for job."""
        query = f'''SELECT group_name, user_name, job.status, whiteboard,
        recipe_set.queue_time FROM job LEFT JOIN tg_group ON
        job.group_id = tg_group.group_id LEFT JOIN recipe_set ON
        recipe_set.job_id = job.id  LEFT JOIN tg_user ON
        tg_user.user_id = job.owner_id WHERE job.id = {job_id};;'''

        rows, _ = self.connector.query(query)

        group_name, owner_name, status, whiteboard, queue_time = rows[0]
        queue_time = sanitize_date(queue_time)

        return group_name, owner_name, status, whiteboard, queue_time

    def get_job_recipes(self, job_id):
        """Query Beaker, get recipe_ids for jobid."""
        query = f'''SELECT recipe.id FROM recipe LEFT JOIN recipe_set
                ON recipe_set.id = recipe.recipe_set_id
                WHERE recipe_set.job_id = {job_id};'''

        rows, _ = self.connector.query(query)

        return [str(x) for x in itertools.chain(*rows)]

    def get_recipeids_duration(self, lst_recipe_ids):
        """Query Beaker, get start, finish time and duration for recipeids."""
        rec_start_times, rec_finish_times, rec_durations = [], [], []
        query = f'''SELECT start_time, finish_time FROM recipe
                WHERE recipe.id IN ({', '.join(lst_recipe_ids)});'''

        rows, _ = self.connector.query(query)

        for start_time, finish_time in rows:
            start_time = sanitize_date(start_time)
            finish_time = sanitize_date(finish_time)
            duration = finish_time - start_time

            rec_start_times.append(start_time)
            rec_finish_times.append(finish_time)
            rec_durations.append(duration)

        return rec_start_times, rec_finish_times, rec_durations

    @classmethod
    def from_jobid(cls, connector, job_id):
        # pylint: disable=R0914
        """Query Beaker, create internal data structure for given job_id."""
        temp = BkrMetricsJob(connector)

        # get all the recipeids in this job
        lst_recipe_ids = temp.get_job_recipes(job_id)

        # get info about how long each recipe took
        rec_start_times, rec_finish_times, rec_durations = \
            temp.get_recipeids_duration(lst_recipe_ids)

        # get remaining attributes
        group_name, owner_name, status, whiteboard, queue_time = \
            temp.get_metrics_part(job_id)

        group_name = group_name if group_name else {}

        statuses, distro_names, arches = \
            temp.get_recipeids_status_distro_arch(lst_recipe_ids)

        # create actual result object
        job = BkrMetricsJob(connector, job_id=job_id,
                            lst_recipe_ids=lst_recipe_ids,
                            statuses=statuses,
                            distro_names=distro_names,
                            arches=arches,
                            rec_start_times=rec_start_times,
                            rec_finish_times=rec_finish_times,
                            rec_durations=rec_durations, group_name=group_name,
                            owner_name=owner_name, status=status,
                            whiteboard=whiteboard, queue_time=queue_time)

        return job

    def get_metrics_tasks(self):
        """Query Beaker, use internal data to get tasks datapoints."""
        results = []
        kwargs = self.kwargs

        data = zip(kwargs['lst_recipe_ids'], kwargs['rec_start_times'],
                   kwargs['arches'], kwargs['distro_names'])
        for recipe_id, rec_start_time, arch, distro in data:
            for task in BkrTask.from_query(self.connector, recipe_id):
                start_time = sanitize_date(task['start_time'])
                finish_time = sanitize_date(task['finish_time'])
                duration = (finish_time - start_time).total_seconds()

                results.append({
                    'measurement': 'tasks',
                    'tags': {
                        'arch': arch,
                        'distro_name': distro,
                        'name': task['name'],
                        'status': task['status'],
                        'result': task['result'],
                        'waived': task['waived'],
                        'group_name': kwargs['group_name'] or {},
                        'owner_name': kwargs['owner_name'],
                    },
                    'fields': {
                        'id': task['task_id'],
                        'job_id': kwargs['job_id'],
                        'recipe_id': recipe_id,
                        'duration': duration,
                    },
                    'time': (rec_start_time or
                             kwargs['queue_time']),
                })
        return results

    def get_beaker_tasks(self, recipe_set_id):
        """Get BkrTask list for given recipe_set_id."""
        results = {}
        query = f'SELECT id FROM recipe WHERE recipe_set_id = {recipe_set_id};'

        rows, _ = self.connector.query(query)
        recipe_id_lst = list(*itertools.chain(rows))
        for recipe_id in recipe_id_lst:
            results[recipe_id] = BkrTask.from_query_filtered(self.connector,
                                                             recipe_id)

        return results
