"""Wrap psycopg2 in such a way, that it's compatible with teiid.py."""
import psycopg2


class PsycopgWrap:
    """Wrap psycopg2 to interface similar to teiid.py."""

    def __init__(self, connection_str):
        """Create the object and connect to the server using connection_str."""
        self.connection_str = connection_str
        self.conn = psycopg2.connect(connection_str)
        self.cursor = None

    def query(self, query):
        """Execute a query against connection kept in this object.

        When PsycopgWrap object is created, the connection is already
        established. The query sets up a cursor that the object keeps using
        from then on.
        For SELECT queries, results are fetched and returned.
        In case of key/contraint violation, the transaction is rolled back
        and an exception is raised. Otherwise, the transaction is committed.

        Arguments:
            query: The SQL query to execute (str)
        Raises:
            psycopg2.errors.UniqueViolation: on key/contraint violation
            possibly other exceptions from psycopg2
        Returns:
            a list of rows, a list of columns

        """
        # pylint: disable=E1101
        rows, cols = None, None
        if not self.cursor:
            self.cursor = self.conn.cursor()

        try:
            self.cursor.execute(query)
        except psycopg2.errors.UniqueViolation:
            self.cursor.execute('ROLLBACK;')
            raise
        else:
            self.conn.commit()

        if query.upper().startswith('SELECT'):
            rows = self.cursor.fetchall()
            cols = [t[0] for t in self.cursor.description]

        return rows, cols

    def __del__(self):
        """Close connection on object desctruction."""
        self.conn.close()
