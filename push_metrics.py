#!/usr/bin/env python3
"""Load data from Beaker teiid, covert and push them to postgresql database."""
import itertools
import math
import sys
from multiprocessing.pool import ThreadPool

import zc.lockfile
from cki_lib.misc import get_env_var_or_raise
from cki_lib.logger import file_logger, truncate_logs
from cki_lib.teiid import TeiidConnector, ThreadPoolConnector

from adapter import insert2pgsql
from beaker import BkrMetricsJob, BkrInstallations
from psycopg_wrap import PsycopgWrap

LOGGER = file_logger(__name__)


def get_processed_jobids(client):
    # type: (TeiidConnector) -> list
    """Get all ids from jobs table."""
    rows, _ = client.query('SELECT id FROM jobs;')

    return [int(x) for x in itertools.chain(*rows)]


def get_datapoints(connector, job_id):
    # pylint: disable=E1101
    """Use connector to query Beaker and get all the datapoints for job_id."""
    point_arrays = []

    bkr_metrics = BkrMetricsJob.from_jobid(connector, job_id)
    if bkr_metrics.status != 'Completed' and bkr_metrics.status != 'Aborted' \
            and bkr_metrics.status != 'Cancelled':
        print(f'Skipping job_id {job_id}, status is {bkr_metrics.status}')
        return point_arrays

    point_arrays.append([bkr_metrics.get_metrics_job_sample()])
    point_arrays.append(bkr_metrics.get_machine_recipes())
    point_arrays.append(bkr_metrics.get_metrics_tasks())
    inst_points = BkrInstallations(connector).from_job(bkr_metrics.kwargs)
    if inst_points:
        point_arrays += inst_points

    point_arrays.append(bkr_metrics.get_resources())

    return point_arrays


def go_thr_pool(thr_pool, connector, jobs):
    """Run get_datapoints() in threads for various jobs (jobids)."""
    res = [thr_pool.apply_async(get_datapoints, (connector, jobid)) for jobid
           in jobs]

    point_arrays = []
    for item in res:
        point_arrays += item.get()

    return res


def get_min_max_id(connector, table):
    """Get min/max index of a table."""
    query = f'SELECT MIN(id) FROM {table};'
    rows, _ = connector.query(query)
    min_id = [int(x) for x in itertools.chain(*rows)][0]

    query = f'SELECT MAX(id) FROM {table};'
    rows, _ = connector.query(query)
    max_id = [int(x) for x in itertools.chain(*rows)][0]

    return min_id, max_id


def get_processing_point(metrics_conn, bkr_teiid, batchsize):
    """Determine where to continue processing Beaker teiid db."""
    metrics_min, metrics_max = get_min_max_id(metrics_conn, 'jobs')

    bkr_min, bkr_max = get_min_max_id(bkr_teiid, 'job')

    if metrics_min != bkr_min:
        print('* Cannot find continuation point. Will scan entire db.')
        return None, None

    if metrics_max >= bkr_max:
        print('* DB is either fully processed or not well-formed. '
              'Will scan entire db.')
        return None, None

    process_batch_top = math.ceil(metrics_max / batchsize)

    return process_batch_top, metrics_max


def main():
    # pylint: disable=R0914
    """Do everything: load data from Beaker teiid, convert them and push."""
    # empty logs
    truncate_logs()

    connection_str = get_env_var_or_raise('PG_CON_STR')
    metrics_conn = PsycopgWrap(connection_str)

    teiid_con_str = get_env_var_or_raise('PG_TEIID_CON_STR')
    bkr_teiid = ThreadPoolConnector(teiid_con_str, size=50)
    thr_pool = ThreadPool(processes=50)

    # get ids that are processed
    rows, _ = metrics_conn.query('SELECT id FROM jobs;')
    processed_jobs = [int(x) for x in itertools.chain(*rows)]

    # get all the jobids that are in db
    LOGGER.info('Fetching Beaker job ids from teiid db')

    temp_bkr = BkrMetricsJob(bkr_teiid)
    top, metrics_max = get_processing_point(metrics_conn, bkr_teiid,
                                            temp_bkr.batchsize)
    if top and metrics_max:
        temp_bkr.set_batch2process(top, metrics_max)
        LOGGER.info(f'Starting at batch {temp_bkr.i}')

    is_done = False
    while not is_done:
        beaker_jobids, is_done = temp_bkr.get_batch_of_ids()

        # filterout what is already in our DB
        beaker_jobids = list(
            filter(lambda e: e not in processed_jobs, beaker_jobids))

        LOGGER.info(f'* Processing {len(beaker_jobids)} jobs...')
        if beaker_jobids:
            # create datapoints to write to db en-masse
            point_arrays = go_thr_pool(thr_pool, bkr_teiid, beaker_jobids)
            point_arrays = [p.get() for p in point_arrays]

            queries = []
            for poi in point_arrays:
                for points in poi:
                    if points:
                        queries += insert2pgsql(points)

            metrics_conn.query('\n'.join(queries))


if __name__ == '__main__':
    try:
        LOCK = zc.lockfile.LockFile('/tmp/bkr-metrics-push')
    except zc.lockfile.LockError:
        print('Sorry, this script can run only 1 instance per machine!')
        sys.exit(1)
    else:
        main()
        LOCK.close()
